package com.cip.pdm.cambio;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DecimalFormat;


public class CambioActivity extends AppCompatActivity {

    private EditText edit_euro, edit_dolar;
    private Button bt_convertir;
    double cambio=1.22600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambio);

        edit_euro=(EditText)findViewById(R.id.edit_euro);
        edit_dolar=(EditText)findViewById(R.id.edit_dolar);
        bt_convertir = (Button)findViewById(R.id.bt_convertir);

        bt_convertir.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick (View v){

                Double euro=Double.parseDouble(edit_euro.getText().toString());
                Double dolar= euro*cambio;
                DecimalFormat formato=new DecimalFormat("#.##");
                edit_dolar.setText(String.valueOf(formato.format(dolar)));

            }
        });

    }


}